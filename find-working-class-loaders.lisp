(defun all-threads ()
  (chain (:static "java.lang.Thread") "getAllStackTraces" "keySet" "toArray"))

(defun find-working-class-loaders (threads)
  (loop for thread across threads
        for loader = (jcall "getContextClassLoader" thread)
        for class = (ignore-errors (java:jclass "dbn" loader))
        when (and (not (null class))
                  (jstatic "x" class))
          collect loader))
