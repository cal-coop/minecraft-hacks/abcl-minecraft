(defvar *minecraft* (minecraft:get-instance (jclass "net.minecraft.client.Minecraft")))
(defvar *player*    (minecraft-fields:player *minecraft*))
(defvar *world*     (minecraft-fields:world *minecraft*))

(defstruct angle pitch yaw)

(defun angle-to (px py pz)
  (let* ((dirx (- (minecraft:get-pos-x *player*) px))
         (diry (- (minecraft:get-pos-y *player*) -1 py))
         (dirz (- (minecraft:get-pos-z *player*) pz))
         (len  (sqrt (+ (expt dirx 2) (expt diry 2) (expt dirz 2)))))
    (make-angle :pitch (* (/ 180 pi) (asin (/ diry len)))
                :yaw (+ 90
                        (* (/ 180 pi)
                           (atan (/ dirz len)
                                 (/ dirx len)))))))

(defun distance-to (entity &optional (entity2 *player*))
  (let ((dirx (- (minecraft:get-pos-x entity2) (minecraft:get-pos-x entity)))
        (diry (- (minecraft:get-pos-y entity2) (minecraft:get-pos-y entity)))
        (dirz (- (minecraft:get-pos-z entity2) (minecraft:get-pos-z entity))))
    (sqrt (+ (expt dirx 2) (expt diry 2) (expt dirz 2)))))

(defun angle-to-entity (entity)
  (angle-to (minecraft:get-pos-x entity) (minecraft:get-pos-y entity)
            (minecraft:get-pos-z entity)))

(defun entity-angle (entity)
  (make-angle :pitch (minecraft-fields:rotation-pitch entity)
              :yaw   (minecraft-fields:rotation-yaw entity)))

(defun entities (distance &key (class "net.minecraft.entity.CreatureEntity"))
  (let ((x (minecraft:get-pos-x *player*))
        (y (minecraft:get-pos-y *player*))
        (z (minecraft:get-pos-z *player*)))
    (java:jcall "toArray"
                (minecraft:get-entities-within-a-a-b-b
                 *world*
                 (java:jclass class)
                 (java:jnew "net.minecraft.util.math.AxisAlignedBB"
                            (- x distance)
                            (- y distance)
                            (- z distance)
                            (+ x distance)
                            (+ y distance)
                            (+ z distance))))))

(defun best (list test &key (key #'identity))
  (if (null list)
      (error "list is empty")
      (let ((best (first list)))
        (dolist (value (rest list))
          (when (funcall test (funcall key value) (funcall key best))
            (setf best value)))
        best)))

(defun look-to (angle)
  (setf (minecraft-fields:rotation-pitch *player*) (angle-pitch angle)
        (minecraft-fields:rotation-yaw *player*)   (angle-yaw angle)))

(defun look-to-closest-entity (distance class)
  (let ((entities (coerce (entities distance :class class) 'list)))
    (unless (null entities)
      (let ((closest (best entities #'< :key #'distance-to)))
        (look-to (angle-to-entity closest))))))

(defun aimbot.exe (&key (period 0.2) (distance 60) (class "net.minecraft.entity.CreatureEntity"))
  (loop
    (look-to-closest-entity distance class)
    (sleep period)))
