(asdf:defsystem :obfuscated-minecraft
  :depends-on (:cl-csv :closer-mop)
  :components ((:file "package")
               (:file "tables")
               (:file "generic-functions")))
