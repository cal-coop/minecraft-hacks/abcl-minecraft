(in-package :obfuscated-minecraft)
(defvar *function-table*)
(defvar *field-table*)

(defun skewer-case<-camel-case (name)
  (if (every (lambda (char)
               (or (upper-case-p char)
                   (char= char #\_)))
             name)
      ;; Format a constant like FOO_BAR as +FOO-BAR+
      (format nil "+~A+" (substitute #\- #\_ name))
      ;; Format a normal name like fooBar as FOO-BAR
      (with-output-to-string (buffer)
        (loop for character across name
              for first-character? = t then nil
              do (if (or first-character?
                         (not (upper-case-p character)))
                     (write-char (char-upcase character) buffer)
                     (progn
                       (write-char #\- buffer)
                       (write-char character buffer)))))))

(defun populate-minecraft-package-methods (pathname)
  "Intern symbols for all methods and set their fdefinitions to be generic functions for them."
  (let ((package (find-package :minecraft)))
    (setf *function-table* (make-hash-table :test 'equal))
    (cl-csv:do-csv (row pathname :skip-first-p t)
      (destructuring-bind (obfuscated-name real-name side documentation)
          row
        (declare (ignore side obfuscated-name documentation))
        (push row (gethash real-name *function-table*))
        (let ((name (skewer-case<-camel-case real-name)))
          (unless (find-symbol name package)
            (let ((symbol (intern name package)))
              (setf (fdefinition symbol)
                    (make-obfuscated-function real-name))
              (export symbol package))))))))

(defun populate-minecraft-package-accessors (pathname)
  "Intern symbols for all fields and set their fdefinitions to be accessors for them." 
  (let ((package (find-package :minecraft-fields)))
    (setf *field-table* (make-hash-table :test 'equal))
    (cl-csv:do-csv (row pathname :skip-first-p t)
      (destructuring-bind (obfuscated-name real-name side documentation)
          row
        (declare (ignore side obfuscated-name documentation))
        (push row (gethash real-name *field-table*))
        (let ((name (skewer-case<-camel-case real-name)))
          (unless (find-symbol name package)
            (let ((symbol (intern name package)))
              (setf (fdefinition symbol)
                    (make-obfuscated-reader real-name)
                    (fdefinition `(setf ,symbol))
                    (make-obfuscated-writer real-name))
              (export symbol package))))))))

(defun make-minecraft-package (method-table-pathname field-table-pathname)
  (unless (and (null (find-package :minecraft))
               (null (find-package :minecraft-fields)))
    (cerror "Delete the current package and create new Minecraft packages."
            "Minecraft packages already exist.")
    (when (find-package :minecraft) (delete-package :minecraft))
    (when (find-package :minecraft-fields) (delete-package :minecraft-fields)))
  (let ((method-package (make-package :minecraft))
        (field-package  (make-package :minecraft-fields)))
    (populate-minecraft-package-methods (pathname method-table-pathname))
    (populate-minecraft-package-accessors (pathname field-table-pathname))
    (values method-package field-package)))

(eval-when (:execute :load-toplevel :compile-toplevel)
  ;; A sad, sad attempt at polymorphic thing-looking-up.
  (defun find-obfuscated-thing (thing-name thing-getter thing-maker table-variable)
    (lambda (subject name)
      (block find-thing
        (let* ((class (if (string= (java:jclass-of subject) "java.lang.Class")
                          subject
                        (java:jclass (java:jclass-of subject))))
               (method-names (funcall thing-getter class)))
          (let ((things
                  (loop for row in (gethash name (symbol-value table-variable))
                        for (obfuscated-name) = row
                        when (member obfuscated-name method-names :test #'string=)
                          collect (funcall thing-maker class obfuscated-name))))
            (if (null things)
                (error "no ~a ~a#~a" thing-name (java:jclass-of subject) name)
                things))))))
  
  (setf (fdefinition 'find-obfuscated-methods)
        (find-obfuscated-thing "method"
                               (lambda (class)
                                 (loop for method across (java:jcall "getMethods" class)
                                       collect (java:jcall "getName" method)))
                               (lambda (class name) name)
                               '*function-table*)
        (fdefinition 'find-obfuscated-field)
        (find-obfuscated-thing "field"
                               (lambda (class)
                                 (loop for field across (java:jcall "getFields" class)
                                       collect (java:jcall "getName" field)))
                               (lambda (class name)
                                 (declare (ignore class))
                                 name)
                               '*field-table*)))

(defun class-methods (class)
  (let* ((class-methods (java:jcall "getMethods" class))
         (method-names (loop for method across class-methods
                             collect (java:jcall "getName" method))))
    (loop for real-name being the hash-keys of *function-table*
          for methods   being the hash-values of *function-table*
          appending (loop for (obsfucated-name) in methods
                          for position = (position obsfucated-name method-names :test #'string=)
                          unless (null position) 
                            do (let* ((method (aref class-methods position))
                                      (parameters (loop for type across (java:jcall "getParameterTypes" method)
                                                        collect (java:jcall "getName" type))))
                                 (format t "~&(~a~{ ~a~}) ; ~a"
                                         (skewer-case<-camel-case real-name)
                                         parameters
                                         (java:jcall "getName" method)))))))
