(in-package :obfuscated-minecraft)

(defun make-obfuscated-function (name)
  (let ((known-things (make-hash-table :test 'equal)))
    (lambda (subject &rest arguments)
      (let ((class (java:jclass-of subject)))
        (multiple-value-bind (methods present?)
            (gethash class known-things)
          (unless present?
            (let ((new-methods (find-obfuscated-methods subject name)))
              (setf (gethash class known-things) new-methods
                    methods new-methods)))
          (block find-java-method
            (if (java:jinstance-of-p subject (java:jclass "java.lang.Class"))
                (dolist (method methods)
                  (handler-case
                      (apply #'java:jcall (apply #'java:jmethod subject method arguments)
                             subject arguments)
                    (java:java-exception (e)
                      (unless (java:jinstance-of-p (java:java-exception-cause e)
                                                   (java:jclass "java.lang.NoSuchMethodException"))
                        (error e)))
                    (:no-error (v)
                      (return-from find-java-method v))))
                (dolist (method methods)
                  (handler-case
                      (apply #'java:jcall method subject arguments)
                    (java:java-exception (e)
                      (unless (java:jinstance-of-p (java:java-exception-cause e)
                                                   (java:jclass "java.lang.NoSuchMethodException"))
                        (error e)))
                    (:no-error (v)
                      (return-from find-java-method v)))))
            (error "No applicable method for ~s on ~s and ~{~s~^, ~}" subject arguments)))))))
(defun make-obfuscated-reader (name)
  (let ((known-things (make-hash-table :test 'equal)))
    (lambda (subject)
      (let ((class (java:jclass-of subject)))
        (multiple-value-bind (field present?)
            (gethash class known-things)
          (unless present?
            (let ((new-field (first (find-obfuscated-field subject
                                                           name))))
              (setf (gethash class known-things)
                    new-field
                    field new-field)))
          (java:jfield field subject))))))
(defun make-obfuscated-writer (name)
  (let ((known-things (make-hash-table :test 'equal)))
    (lambda (new-value subject)
      (let ((class (java:jclass-of subject)))
        (multiple-value-bind (field present?)
            (gethash class known-things)
          (unless present?
            (let ((new-field (first (find-obfuscated-field subject
                                                           name))))
              (setf (gethash class known-things)
                    new-field
                    field new-field)))
          (setf (java:jfield field subject) new-value)
          new-value)))))
