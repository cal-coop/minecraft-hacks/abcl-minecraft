(defpackage :obfuscated-minecraft
  (:use :cl)
  (:export #:make-minecraft-package
           #:class-methods))
