package org.cooperativeofappliedlanguage.abclminecraft;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraft.client.Minecraft;
import org.armedbear.lisp.Function;
import org.armedbear.lisp.Interpreter;
import org.armedbear.lisp.Package;
import org.armedbear.lisp.Packages;
import org.armedbear.lisp.Symbol;
import org.armedbear.lisp.JavaObject;
import java.lang.Thread;

@Mod("abclminecraft")
public class ABCLMinecraft {
    Interpreter interpreter;

    public ABCLMinecraft () {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientSetup);
    }
    
    private void setup(final FMLCommonSetupEvent ev) {
        System.out.println("Starting ABCL...");
        interpreter = Interpreter.createInstance();
        try {
            /*
            interpreter.eval("(defvar *minecraft*)");
            interpreter.eval("(defun set-minecraft (minecraft) (setf *minecraft* minecraft))");
            */
            
            interpreter.eval("(load (merge-pathnames \"quicklisp/setup.lisp\" (user-homedir-pathname)))");
            interpreter.eval("(ql:quickload :swank)");
            interpreter.eval("(swank:create-server)");
        } catch (Throwable t) {
            System.out.println("Exception!");
            t.printStackTrace();
            throw t;
        }
    }

    private void clientSetup(final FMLClientSetupEvent event) {
        /*
        Package p = Packages.findPackage("CL-USER");
        Symbol setMinecraftSymbol = p.findAccessibleSymbol("SET-MINECRAFT");
        Function setMinecraft = (Function)setMinecraftSymbol.getSymbolFunction();
        setMinecraft.execute(new JavaObject(Minecraft.getInstance()));
        */
    }
}
