# ABCL in Minecraft

This is a mod that embeds [Armed Bear Common Lisp](https://abcl.org/) in
Minecraft.

## Usage

1. Download `abcl.jar` and put it in `libs/`
2. Build the mod: `./gradlew build`
3. Copy it: `cp build/libs/abcl/abclminecraft-1.0.jar ~/.minecraft/mods`
4. Launch Minecraft
5. Wait....
6. Wait some more.....
7. `M-x slime-connect` with Host: localhost and Port: 4005
8. Download deobsfucation tables from http://export.mcpbot.bspk.rs/
9. `(ql:quickload :obfuscated-minecraft)`
10. `(obfuscated-minecraft:make-minecraft-package #p"<table directory>/methods.csv" #p"<table directory>/fields.csv")`


If something goes wrong, try evaluating these forms in an ABCL REPL, which can
be started using the command `java -jar libs/abcl.jar`, and debug from there:

```lisp
(load (merge-pathnames "quicklisp/setup.lisp" (user-homedir-pathname)))
(ql:quickload :swank)
(swank:create-server)
```
